﻿/*
Name: 			Core Initializer
Written by: 	Okler Themes - (http://www.okler.net)
Version: 		3.1.1
*/

(function() {

	"use strict";

	var Core = {

		initialized: false,

		initialize: function() {

			if (this.initialized) return;
			this.initialized = true;

			// Owl Carousel
			this.owlCarousel();
			this.parallax();
			this.fileinput();

		},
		owlCarousel: function(options) {

			var total = $("div.owl-carousel:not(.manual)").length,
				count = 0;

			$("div.owl-carousel:not(.manual)").each(function() {

				var slider = $(this);

				var defaults = {
					 // Most important owl features
					items : 5,
					itemsCustom : false,
					itemsDesktop : [1199,4],
					itemsDesktopSmall : [980,3],
					itemsTablet: [768,2],
					itemsTabletSmall: false,
					itemsMobile : [479,1],
					singleItem : true,
					itemsScaleUp : false,

					//Basic Speeds
					slideSpeed : 200,
					paginationSpeed : 800,
					rewindSpeed : 1000,

					//Autoplay
					autoPlay : false,
					stopOnHover : false,

					// Navigation
					navigation : false,
					navigationText : ["<i class=\"icon icon-chevron-left\"></i>","<i class=\"icon icon-chevron-right\"></i>"],
					rewindNav : true,
					scrollPerPage : false,

					//Pagination
					pagination : true,
					paginationNumbers: false,

					// Responsive
					responsive: true,
					responsiveRefreshRate : 200,
					responsiveBaseWidth: window,

					// CSS Styles
					baseClass : "owl-carousel",
					theme : "owl-theme",

					//Lazy load
					lazyLoad : false,
					lazyFollow : true,
					lazyEffect : "fade",

					//Auto height
					autoHeight : false,

					//JSON
					jsonPath : false,
					jsonSuccess : false,

					//Mouse Events
					dragBeforeAnimFinish : true,
					mouseDrag : true,
					touchDrag : true,

					//Transitions
					transitionStyle : false,

					// Other
					addClassActive : false,

					//Callbacks
					beforeUpdate : false,
					afterUpdate : false,
					beforeInit: false,
					afterInit: false,
					beforeMove: false,
					afterMove: false,
					afterAction: false,
					startDragging : false,
					afterLazyLoad : false
				}

				var config = $.extend({}, defaults, options, slider.data("plugin-options"));

				// Initialize Slider
				slider.owlCarousel(config).addClass("owl-carousel-init");

			});

		},
		parallax: function() {
		    if(typeof($.stellar) == "undefined") {
		        return false;
		    }

		    $(window).load(function () {

		        if($(".parallax").get(0)) {
		            if(!Modernizr.touch) {
		                $(window).stellar({
		                    responsive:true,
		                    scrollProperty: 'scroll',
		                    parallaxElements: false,
		                    horizontalScrolling: false,
		                    horizontalOffset: 0,
		                    verticalOffset: 0
		                });
		            } else {
		                $(".parallax").addClass("disabled");
		            }
		        }
		    });
		},

		fileinput: function() {
			$(document).ready(function () {
			    $("input:file").change(function () {
			        var fileName = $(this).val();
			        $(this).next().val(fileName);
			    });
			});
		}



	};

	Core.initialize();
})();
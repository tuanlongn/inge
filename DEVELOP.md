### DEVELOP MANUAL ###

# Create new bundle:
- $ php app/console generate:bundle --namespace=Inge/NewBundle

# Generate repository:
- $ php app/console generate:doctrine:entities IngeNewBundle
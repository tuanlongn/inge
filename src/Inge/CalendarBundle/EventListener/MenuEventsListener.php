<?php

namespace Inge\CalendarBundle\EventListener;

use Inge\AdminBundle\Event\ConfigureMenuEvent;

class MenuEventsListener
{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Inge\AdminBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureRoot($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $menu = $factory->createItem('calendar', array('route' => 'admin_calendar'));
        $menu->setAttribute('icon', 'fa fa-calendar');
        $menu->setExtra('orderNumber', 4);
        $parentMenu->addChild($menu);
    }
}
<?php

namespace Inge\CalendarBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\EventBundle\Entity\Event;

class CalendarController extends AdminController
{
    /**
     * @Route("/admin/calendar", name="admin_calendar")
     * @Template("IngeCalendarBundle::calendar.html.twig")
     */
    public function indexAction()
    {
        $this->checkPermission();

        $publishedEvent = $this->getPublishedEvent();
        $unpublishedEvent = $this->getUnpublishedEvent();

        return array(
            'publishedEvent' => $publishedEvent, 
            'unpublishedEvent' => $unpublishedEvent
        );
    }

    /**
     * @Route("/admin/calendar/update", name="admin_calendar_update")
     * @Template()
     */
    public function calendarUpdateAction(Request $request)
    {
        $this->checkPermission();

        $data = $request->request->get('data');
        foreach ($data as $item) {
            $event = $this->getDoctrine()->getRepository('IngeEventBundle:Event')->findOneById($item['id']);
            $startTime = new \DateTime($item['start_at']);
            if (empty($item['end_at'])) {
                $endTime = new \DateTime($item['start_at']);
            } else {
                $endTime = new \DateTime($item['end_at']);
            }
            
            $event->setStartAt($startTime);
            $event->setEndAt($endTime);
            $event->setPublish(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    public function getPublishedEvent()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('
            SELECT e FROM IngeEventBundle:Event e WHERE e.publish = 1 AND e.start_at IS NOT NULL AND e.end_at IS NOT NULL
        ');
        return $query->getResult();
    }

    public function getUnpublishedEvent()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('
            SELECT e FROM IngeEventBundle:Event e WHERE e.publish = 0 OR e.start_at IS NULL OR e.end_at IS NULL
        ');
        return $query->getResult();
    }
}

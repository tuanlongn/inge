<?php

namespace Inge\UserBundle\EventListener;

use Inge\AdminBundle\Event\ConfigureMenuEvent;

class MenuEventsListener
{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Inge\AdminBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureRoot($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $menu = $factory->createItem('user', array('route' => 'admin_user_list'));
        $menu->setAttribute('icon', 'fa fa-user');
        $menu->setExtra('orderNumber', 2);
        $parentMenu->addChild($menu);
    }
}
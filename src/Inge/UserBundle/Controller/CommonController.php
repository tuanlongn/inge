<?php

namespace Inge\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommonController extends Controller
{
    /**
     * @Route("/common/get_users", name="get_users")
     * @Template()
     */
    public function getUsersAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/common/get_all_users", name="get_all_users")
     * @Template()
     */
    public function getAllUsersAction(Request $request)
    {
        $data = array();
        $users = $this->getDoctrine()->getRepository('IngeUserBundle:User')->findAll();
        foreach ($users as $user) {
            $data[] = array(
                'type' => 'user',
                'name' => $user->getUsername(),
                'id' => $user->getId()
            );
        }

        $groups = $this->getDoctrine()->getRepository('IngeUserBundle:UserGroup')->findAll();
        foreach ($groups as $group) {
            $data[] = array(
                'type' => 'group',
                'name' => $group->getName(),
                'id' => $group->getId()
            );
        }

        $q = $request->get('q');
        if ($q !== '') {
            foreach ($data as $k => $v) {
                if (strpos(strtolower($v['name']), strtolower($q)) === false) {
                    unset($data[$k]);
                }
            }
        }

        $exist = $request->get('exist');
        if (empty($exist) === false) {
            foreach ($exist as $key) {
                foreach ($data as $k => $v) {
                    if (urlencode($key) === $k) {
                        unset($data[$k]);
                    }
                }
            }
        }

        $response = new JsonResponse();
        $response->setData(array(
            'data' => $data
        ));
        return $response;
    }
}

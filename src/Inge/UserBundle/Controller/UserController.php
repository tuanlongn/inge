<?php

namespace Inge\UserBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Inge\UserBundle\Entity\User;
use Inge\UserBundle\Entity\UserGroup;

class UserController extends AdminController
{
    // USER ////////////////////////////////////////////////
    // -----------------------------------------------------

    /**
     * @Route("/admin/user", name="admin_user_list")
     * @Template("IngeUserBundle::user_list.html.twig")
     */
    public function userListAction()
    {
        $this->checkPermission();
        
        $users = $this->getDoctrine()->getRepository('IngeUserBundle:User')->findAll();

        return array(
            'users' => $users
        );
    }

    /**
     * @Route("/admin/user/add", name="admin_user_add")
     * @Template("IngeUserBundle::user_form.html.twig")
     */
    public function userAddAction(Request $request)
    {
        $this->checkPermission();

        $user = new User();

        if ($request->getMethod() === 'POST') {
            $user->setUsername($request->request->get('username'));
            $user->setPassword($request->request->get('password'));

            $email = $request->request->get('email');
            $checkUniqueEmail = $this->getDoctrine()->getRepository('IngeUserBundle:User')
                                ->checkUniqueEmail($email);
            if ($checkUniqueEmail === false) {
                $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('email already used by other person'));
            } else {
                $user->setEmail($email);

                $group = $this->getDoctrine()->getRepository('IngeUserBundle:UserGroup')  
                        ->findOneById($request->request->get('group'));
                $user->setGroup($group);

                $user->setFullname($request->request->get('fullname'));
                $user->setPhonenumber($request->request->get('phonenumber'));
                $user->setAddress($request->request->get('address'));
                $user->setRoles($request->request->get('roles'));

                $status = $request->request->get('status');
                if ($status) {
                    $status = true;
                } else {
                    $status = false;
                }
                $user->setStatus($status);
                $user->setSalt(md5(time()));

                // generate password
                $encoderFactory = $this->get('security.encoder_factory');
                $encoder = $encoderFactory->getEncoder($user);
                $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($password);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->redirect($this->generateUrl('admin_user_list'));
            }
        }

        $groups = $this->getDoctrine()
                       ->getRepository('IngeUserBundle:UserGroup')
                       ->findAll();

        return array(
            'type' => 'add',
            'groups' => $groups,
            'user' => $user
        );
    }

    /**
     * @Route("/admin/user/{id}/edit", name="admin_user_edit")
     * @Template("IngeUserBundle::user_form.html.twig")
     */
    public function userEditAction($id, Request $request)
    {
        $this->checkPermission();

        $user = $this
                    ->getDoctrine()
                    ->getRepository('IngeUserBundle:User')
                    ->findOneById($id);

        $groups = $this
                    ->getDoctrine()
                    ->getRepository('IngeUserBundle:UserGroup')
                    ->findAll();

        if ($request->getMethod() === 'POST') {
            $password = $request->request->get('password');
            if ($password) {
                // generate new password
                $encoderFactory = $this->get('security.encoder_factory');
                $encoder = $encoderFactory->getEncoder($user);
                $newPass = $encoder->encodePassword($password, $user->getSalt());
                $user->setPassword($newPass);
            }

            $group = $this
                        ->getDoctrine()
                        ->getRepository('IngeUserBundle:UserGroup')  
                        ->findOneById($request->request->get('group'));
            $user->setGroup($group);

            $user->setFullname($request->request->get('fullname'));
            $user->setPhonenumber($request->request->get('phonenumber'));
            $user->setAddress($request->request->get('address'));
            $user->setRoles($request->request->get('roles'));

            $status = $request->request->get('status');
            if ($status) {
                $status = 1;
            } else {
                $status = 0;
            }

            $user->setStatus($status);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return array(
            'type' => 'edit',
            'groups' => $groups,
            'user' => $user
        );
    }

    /**
     * @Route("/admin/user/{id}/delete", name="admin_user_delete")
     * @Template("IngeUserBundle::user_list.html.twig")
     */
    public function userDeleteAction($id)
    {
        $this->checkPermission();
        return array();
    }

    // USER_GROUP //////////////////////////////////////////
    // -----------------------------------------------------

    /**
     * @Route("/admin/user_group", name="admin_user_group_list")
     * @Template("IngeUserBundle::user_group_list.html.twig")
     */
    public function userGroupListAction()
    {
        $this->checkPermission();
        
        $groups = $this->getDoctrine()
                       ->getRepository('IngeUserBundle:UserGroup')
                       ->findAll();

        return array(
            'groups' => $groups
        );
    }

    /**
     * @Route("/admin/user_group/add", name="admin_user_group_add")
     * @Template("IngeUserBundle::user_group_form.html.twig")
     */
    public function userGroupAddAction(Request $request)
    {
        $this->checkPermission();

        $group = new UserGroup();

        if ($request->getMethod() === 'POST') {
            $group->setName($request->request->get('name'));
            $group->setRoles($request->request->get('roles'));

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $group->setStatus($status);

            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_user_group_list'));
        }

        return array(
            'action' => 'add',
            'group' => $group
        );
    }

    /**
     * @Route("/admin/user_group/{id}/edit", name="admin_user_group_edit")
     * @Template("IngeUserBundle::user_group_form.html.twig")
     */
    public function userGroupEditAction(Request $request, $id)
    {
        $this->checkPermission();

        $group = $this->getDoctrine()->getRepository('IngeUserBundle:UserGroup')->findOneById($id);

        if ($request->getMethod() === 'POST') {
            $group->setName($request->request->get('name'));
            $group->setRoles($request->request->get('roles'));

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $group->setStatus($status);

            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();
        }

        return array(
            'action' => 'edit',
            'group' => $group
        );
    }

    /**
     * @Route("/admin/user_group/{id}/delete", name="admin_user_group_delete")
     * @Template("IngeUserBundle::user_list.html.twig")
     */
    public function userGroupDeleteAction($id)
    {
        $this->checkPermission();
        return array();
    }

    // IMPORT & EXPORT //:::::::::::::::::::::::::::::::::>>
    // --------------------------------------------------->>

    /**
     * @Route("/admin/user/import", name="admin_user_import")
     * @Template("IngeUserBundle::import.html.twig")
     */
    public function importAction()
    {
        $this->checkPermission();

        $mappings = $this->getDoctrine()->getManager()->getClassMetadata('IngeUserBundle:User');
        $fields = $mappings->getFieldNames();
        foreach ($fields as $i => $field) {
            switch ($field) {
                case 'id':
                case 'salt':
                case 'password':
                case 'roles':
                case 'created_at':
                case 'modified_at':
                    unset($fields[$i]);
                    break;
            }
        }
        return array(
            'fields' => $fields
        );
    }

    /**
     * @Route("/admin/user/export", name="admin_user_export")
     * @Template("IngeUserBundle::user_list.html.twig")
     */
    public function exportAction()
    {
        $this->checkPermission();
        return array();
    }
}

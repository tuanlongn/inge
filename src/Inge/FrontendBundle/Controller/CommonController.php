<?php

namespace Inge\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommonController extends Controller
{
    /**
     * @Route("/util/get_menu", name="get_menu")
     * @Template("IngeFrontendBundle:base:menu.html.twig")
     */
    public function getMenuAction()
    {
        $menuSetting = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord('frontend', 'menu');
        if ($menuSetting) {
            $menus = json_decode($menuSetting->getValue(), true);
        } else {
            $menus = array();
        }

        return array(
            'menus' => $menus
        );
    }
}

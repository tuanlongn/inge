<?php

namespace Inge\FrontendBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\FrontendBundle\Entity\Menu;
use Inge\AdminBundle\Entity\Setting;

class SettingController extends AdminController
{
    /**
     * @Route("/admin/setting/frontend_menu", name="admin_setting_frontend_menu")
     * @Template("IngeFrontendBundle:components:setting_menu.html.twig")
     */
    public function indexAction()
    {
        $this->checkPermission();

        $routes = $this->getAllRoutes();

        $menus = $this->getDoctrine()
                    ->getRepository('IngeFrontendBundle:Menu')
                    ->findAll();

        $menuSetting = $this->getDoctrine()
                        ->getRepository('IngeAdminBundle:Setting')
                        ->findSettingRecord('frontend', 'menu');
        if ($menuSetting) {
            $menuSetting = json_decode($menuSetting->getValue(), true);
            foreach ($menus as $k2 => $m2) {
                foreach ($menuSetting as $k1 => $m1) {
                    if ($m1['id'] === $m2->getId()) {
                        unset($menus[$k2]);
                    }

                    if (array_key_exists('children', $m1)) {
                        foreach ($m1['children'] as $m3) {
                            if ($m3['id'] === $m2->getId()) {
                                unset($menus[$k2]);
                            }
                        }
                    }
                }
            }
        } else {
            $menuSetting = array();
        }

        return array(
            'routes' => $routes,
            'menus' => $menus,
            'menuSetting' => $menuSetting
        );
    }

    /**
     * @Route("/admin/menu/add", name="admin_menu_add")
     * @Template()
     */
    public function menuAddAction(Request $request)
    {
        $menu = new Menu();

        $title = $request->request->get('title');
        $menu->setTitle($title);

        $link = $this->generateUrl($request->request->get('link'));
        $menu->setLink($link);

        $em = $this->getDoctrine()->getManager();
        $em->persist($menu);
        $em->flush();

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true,
            'data' => array(
                'id' => $menu->getId(),
                'title' => $title,
                'link' => $link
            )
        ));
        return $response;
    }

    /**
     * @Route("/admin/menu/delete", name="admin_menu_delete")
     * @Template()
     */
    public function menuDeleteAction(Request $request)
    {
        $menu = $this->getDoctrine()
                    ->getRepository('IngeFrontendBundle:Menu')
                    ->findOneById($request->request->get('id'));

        if ($menu) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($menu);
            $em->flush();
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    /**
     * @Route("/admin/menu/update", name="admin_menu_update")
     * @Template()
     */
    public function menuSettingUpdateAction(Request $request)
    {
        $setting = $this->getDoctrine()
                    ->getRepository('IngeAdminBundle:Setting')
                    ->findSettingRecord('frontend', 'menu');

        if ($setting === false) {
            $setting = new Setting();
            $setting->setArea('frontend');
            $setting->setType('menu');
        }

        $setting->setValue($request->request->get('data'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($setting);
        $em->flush();

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    private function getAllRoutes()
    {
        $routes = array();
        $allRoutes = $this->container->get('router')->getRouteCollection()->all();
        foreach ($allRoutes as $routeName => $routeData) {
            if (strpos($routeName, 'frontend') !== false) {
                $routes[] = $routeName;
            }
        }
        return $routes;
    }
}

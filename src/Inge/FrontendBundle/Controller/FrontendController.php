<?php

namespace Inge\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class FrontendController extends Controller
{
    /**
     * @Route("/", name="frontend_home")
     * @Template("IngeFrontendBundle::home.html.twig")
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/servizi", name="frontend_servizi")
     * @Template("IngeFrontendBundle::servizi.html.twig")
     */
    public function serviziAction()
    {
        return array();
    }

    /**
     * @Route("/commissioni", name="frontend_commissioni")
     * @Template("IngeFrontendBundle::commissioni.html.twig")
     */
    public function commissioniAction()
    {
        return array();
    }

    /**
     * @Route("/commissioni_scambio", name="frontend_commissioni_scambio")
     * @Template("IngeFrontendBundle::commissioni_scambio.html.twig")
     */
    public function commissioniScambioAction()
    {
        return array();
    }

    /**
     * @Route("/commissioni_download", name="frontend_commissioni_download")
     * @Template("IngeFrontendBundle::commissioni_download.html.twig")
     */
    public function commissioniDownloadAction()
    {
        return array();
    }

    /**
     * @Route("/commissioni_memberi", name="frontend_commissioni_memberi")
     * @Template("IngeFrontendBundle::commissioni_memberi.html.twig")
     */
    public function commissioniMemberiAction()
    {
        return array();
    }

    /**
     * @Route("/commissioni_mansionari", name="frontend_commissioni_mansionari")
     * @Template("IngeFrontendBundle::commissioni_mansionari.html.twig")
     */
    public function commissioniMansionariAction()
    {
        return array();
    }

    /**
     * @Route("/commissioni_elenco", name="frontend_commissioni_elenco")
     * @Template("IngeFrontendBundle::commissioni_elenco.html.twig")
     */
    public function commissioniElencoAction()
    {
        return array();
    }

    /**
     * @Route("/news", name="frontend_news")
     * @Template("IngeFrontendBundle::news.html.twig")
     */
    public function newsAction()
    {
        $articles = $this->getDoctrine()->getRepository('IngeArticleBundle:Article')->findAll();

        $article = $this->getDoctrine()->getRepository('IngeArticleBundle:Article')->findOneByStatus(array('status' => true));
        
        $categories = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->findByStatus(array('status' => true));

        return array(
            'articles' => $articles,
            'article' => $article,
            'categories' => $categories
        );
    }

    /**
     * @Route("/calendario", name="frontend_calendar")
     * @Template("IngeFrontendBundle::calendar.html.twig")
     */
    public function calendarAction()
    {
        return array();
    }

    /**
     * @Route("/storica", name="frontend_storica")
     * @Template("IngeFrontendBundle::storica.html.twig")
     */
    public function storicaAction()
    {
        return array();
    }

    /**
     * @Route("/press", name="frontend_press")
     * @Template("IngeFrontendBundle::press.html.twig")
     */
    public function pressAction()
    {
        return array();
    }

    /**
     * @Route("/faq", name="frontend_faq")
     * @Template("IngeFrontendBundle::faq.html.twig")
     */
    public function faqAction()
    {
        return array();
    }

    /**
     * @Route("/enti", name="frontend_enti")
     * @Template("IngeFrontendBundle::enti.html.twig")
     */
    public function entiAction()
    {
        return array();
    }
}

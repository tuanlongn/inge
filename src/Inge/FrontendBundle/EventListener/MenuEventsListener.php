<?php

namespace Inge\FrontendBundle\EventListener;

use Inge\AdminBundle\Event\ConfigureMenuEvent;

class MenuEventsListener
{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Inge\AdminBundle\Event\ConfigureMenuEvent $event
     */
    public function menuConfigureSettingNode($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $menu = $factory->createItem('frontend menu', array('route' => 'admin_setting_frontend_menu'));
        $menu->setAttribute('icon', 'fa fa-sitemap');
        $menu->setExtra('orderNumber', 1);
        $parentMenu->addChild($menu);
    }
}
<?php

namespace Inge\MediaBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * MediaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MediaRepository extends EntityRepository
{
    public function findAllFiles()
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.type != :type')->setParameter('type', 'youtube');

        return $qb->getQuery()->getResult();
    }
}

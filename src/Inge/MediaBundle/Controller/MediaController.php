<?php

namespace Inge\MediaBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\MediaBundle\Entity\Media;

class MediaController extends AdminController
{
    /**
     * @Route("/admin/media", name="admin_media")
     * @Template("IngeMediaBundle::index.html.twig")
     */
    public function indexAction()
    {
        $youtubes = $this->getDoctrine()->getRepository('IngeMediaBundle:Media')->findByType('youtube');
        $files = $this->getDoctrine()->getRepository('IngeMediaBundle:Media')->findAllFiles();

        $extensions = array();
        $mt = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord('backend', 'mime_type_upload');
        if ($mt) {
            $tmp = json_decode($mt->getValue(), true);
            foreach ($tmp as $k => $v) {
                $extensions[$k] = $v['text'];
            }
        }

        $maxSize = 0;
        $ms = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord('backend', 'max_size_upload');
        if ($ms) {
            $maxSize = intval($ms->getValue());
        }

        return array(
            'youtubes' => $youtubes,
            'extensions' => $extensions,
            'maxSize' => $maxSize,
            'files' => $files
        );
    }

    /**
     * @Route("/admin/media/add", name="admin_media_add")
     * @Template()
     */
    public function mediaAddAction(Request $request)
    {
        $uploadDir = __DIR__ . '/../../../../web/uploads';
        $media = new Media();
        $flg = false;

        $type = $request->request->get('type');

        if ($type === 'youtube') {
            $name = $request->request->get('name');
            $value = $request->request->get('value');

            $msg = $this->get('translator')->trans('add youtube video successful.');
            $this->get('session')->getFlashBag()->add('success', $msg);

            $flg = true;

        } elseif ($type === 'file') {
            $file = $request->files->get('file');
            $clientMimeType = $file->getClientMimeType();
            $clientFileName = $file->getClientOriginalName();
            
            $checkMimeType = false;

            $tmp = array();
            $mt = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord('backend', 'mime_type_upload');
            if ($mt) {
                $tmp = json_decode($mt->getValue(), true);
            }
            foreach ($tmp as $mt) {
                if (strtolower($clientMimeType) === strtolower($mt['id'])) {
                    $checkMimeType = true;
                    break;
                }
            }

            if ($checkMimeType === true) {
                $file->move($uploadDir . '/' . $mt['text'], $clientFileName);

                $msg = $this->get('translator')->trans('upload file '.$clientFileName.' successful.');
                $this->get('session')->getFlashBag()->add('success', $msg);

                $type = $mt['text'];
                $name = str_replace('.' . $mt['text'], '', $clientFileName);
                $value = '/uploads/' . $mt['text'] . '/' .$clientFileName;
                $flg = true;
            }
        }

        if ($flg) {
            $media->setType($type);
            $media->setName($name);
            $media->setValue($value);

            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_media'));
    }


    /**
     * @Route("/admin/media/{id}/delete", name="admin_media_delete")
     * @Template()
     */
    public function mediaDeleteAction(Request $request, $id)
    {
        $media = $this->getDoctrine()->getRepository('IngeMediaBundle:Media')->findOneById($id);
        if ($media) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($media);
            $em->flush();
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }
}

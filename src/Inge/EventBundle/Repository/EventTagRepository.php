<?php

namespace Inge\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * EventTagRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EventTagRepository extends EntityRepository
{
    public function findById($param)
    {
        if (is_string($param)) {
            $ids = json_decode($param, true);
        } else {
            $ids = $param;
        }
        $query = $this->getEntityManager()->createQuery("
            SELECT t FROM IngeEventBundle:EventTag t WHERE t.id IN (:tagIds)
        ");
        $query->setParameter('tagIds', $ids);
        $result = $query->getResult();
        return ($result) ? $result : false;
    }
}

<?php

namespace Inge\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="event_logs")
 * @ORM\Entity(repositoryClass="Inge\EventBundle\Repository\EventLogRepository")
 */
class EventLog
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $log;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $log_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return EventLog
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set log
     *
     * @param string $log
     * @return EventLog
     */
    public function setLog($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log
     *
     * @return string 
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Set log_at
     *
     * @param \DateTime $logAt
     * @return EventLog
     */
    public function setLogAt($logAt)
    {
        $this->log_at = $logAt;

        return $this;
    }

    /**
     * Get log_at
     *
     * @return \DateTime 
     */
    public function getLogAt()
    {
        return $this->log_at;
    }
}

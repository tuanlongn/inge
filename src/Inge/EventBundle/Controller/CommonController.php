<?php

namespace Inge\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommonController extends Controller
{
    /**
     * @Route("/common/get_all_tags", name="get_all_tags")
     * @Template()
     */
    public function getAllTagsAction(Request $request)
    {
        $data = array();
        $ets = $this->getDoctrine()->getRepository('IngeEventBundle:EventTag')->findAll();
        if ($ets) {
            foreach ($ets as $et) {
                $data[] = array(
                    'id' => $et->getId(),
                    'name' => $et->getName()
                );
            }
        }

        $q = $request->get('q');
        if ($q !== '') {
            foreach ($data as $k => $v) {
                if (strpos(strtolower($v['name']), strtolower($q)) === false) {
                    unset($data[$k]);
                }
            }
        }

        $exist = $request->get('exist');
        if (empty($exist) === false) {
            foreach ($exist as $key) {
                foreach ($data as $k => $v) {
                    if (urlencode($key) === $k) {
                        unset($data[$k]);
                    }
                }
            }
        }

        $response = new JsonResponse();
        $response->setData(array(
            'data' => $data
        ));
        return $response;
    }
}
<?php

namespace Inge\EventBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\EventBundle\Entity\Event;
use Inge\EventBundle\Entity\EventTag;

class EventController extends AdminController
{
    /**
     * @Route("/admin/event", name="admin_event")
     * @Template("IngeEventBundle::event_list.html.twig")
     */
    public function indexAction()
    {
        $this->checkPermission();

        $events = $this->getDoctrine()->getRepository('IngeEventBundle:Event')->findAll();

        return array(
            'events' => $events
        );
    }

    /**
     * @Route("/admin/event/add", name="admin_event_add")
     * @Template("IngeEventBundle::event_form.html.twig")
     */
    public function addAction(Request $request)
    {
        $this->checkPermission();

        $event = new Event();

        if ($request->getMethod() === 'POST') {
            $em = $this->getDoctrine()->getManager();

            $event->setName($request->request->get('name'));
            $event->setColor($request->request->get('color'));
            if ($request->request->get('start_at') !== '') {
                $startTime = new \DateTime($request->request->get('start_at'));
                $endTime = new \DateTime($request->request->get('end_at'));
                $event->setStartAt($startTime);
                $event->setEndAt($endTime);
            }

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $event->setPublish($status);

            // tags
            $eventTagRepository = $this->getDoctrine()->getRepository('IngeEventBundle:EventTag');
            $tags = explode(',', $request->request->get('tags'));
            if (!empty($tags)) {
                $tagIds = array();
                foreach ($tags as $tag) {
                    $tmp = json_decode(urldecode($tag), true);
                    if (is_array($tmp)) {
                        $data = $eventTagRepository->findOneById($tmp['id']);
                        $tagId = $data->getId();
                    } else if (!empty($tmp)) {
                        $newEventTag = new EventTag();
                        $newEventTag->setName($tag);
                        $em->persist($newEventTag);
                        $em->flush();

                        $tagId = $newEventTag->getId();
                    } else {
                        continue;
                    }
                    $tagIds[] = $tagId;
                }
            }
            $event->setTags(json_encode($tagIds, true));

            // users
            $users = $request->request->get('users');
            $event->setUsers('['.urldecode($users).']');
            
            $em->persist($event);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_event'));
        }
        
        return array(
            'action' => 'add',
            'event' => $event
        );
    }

    /**
     * @Route("/admin/event/{id}/edit", name="admin_event_edit")
     * @Template("IngeEventBundle::event_form.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $event = $this->getDoctrine()->getRepository('IngeEventBundle:Event')->findOneById($id);

        if ($request->getMethod() === 'POST') {
            $em = $this->getDoctrine()->getManager();

            $event->setName($request->request->get('name'));
            $event->setColor($request->request->get('color'));
            if ($request->request->get('start_at') !== '') {
                $startTime = new \DateTime($request->request->get('start_at'));
                $endTime = new \DateTime($request->request->get('end_at'));
                $event->setStartAt($startTime);
                $event->setEndAt($endTime);
            }

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $event->setPublish($status);

            // tags
            $eventTagRepository = $this->getDoctrine()->getRepository('IngeEventBundle:EventTag');
            $tags = explode(',', $request->request->get('tags'));
            if (!empty($tags)) {
                $tagIds = array();
                foreach ($tags as $tag) {
                    $tmp = json_decode(urldecode($tag), true);
                    if (is_array($tmp)) {
                        $data = $eventTagRepository->findOneById($tmp['id']);
                        $tagId = $data->getId();
                    } else {
                        $newEventTag = new EventTag();
                        $newEventTag->setName($tag);
                        $em->persist($newEventTag);
                        $em->flush();

                        $tagId = $newEventTag->getId();
                    }
                    $tagIds[] = $tagId;
                }
            }
            $event->setTags(json_encode($tagIds, true));

            // users
            $users = $request->request->get('users');
            $event->setUsers('['.urldecode($users).']');
            
            $em->persist($event);
            $em->flush();

            $msg = $this->get('translator')->trans('update success.');
            $this->get('session')->getFlashBag()->add('success', $msg);
        }

        // get data tags
        $tmp = array();
        $tags = $this->getDoctrine()->getRepository('IngeEventBundle:EventTag')->findById($event->getTags());
        if ($tags) {
            foreach ($tags as $tag) {
                $tmp[] = array(
                    'id' => $tag->getId(),
                    'name' => $tag->getName()
                );
            }
        }
        $event->setTags(json_encode($tmp, true));

        return array(
            'action' => 'edit',
            'event' => $event
        );
    }

    /**
     * @Route("/admin/event/{id}/delete", name="admin_event_delete")
     * @Template()
     */
    public function deleteAction($id)
    {
        $this->checkPermission();

        $event = $this->getDoctrine()->getRepository('IngeEventBundle:Event')->findOneById($id);
        if ($event) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
        }
        
        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    // EVENT CATEGORY //////////////////////////////////////
    ////////////////////////////////////////////////////////
    /**
     * @Route("/admin/event/category", name="admin_event_category")
     * @Template("IngeEventBundle::event_tag_list.html.twig")
     */
    public function eventCategoryListAction()
    {
        $this->checkPermission();

        $eventTags = $this->getDoctrine()->getRepository('IngeEventBundle:EventTag')->findAll();
        return array(
            'tags' => $eventTags
        );
    }

    /**
     * @Route("/admin/event/category/{id}/delete", name="admin_event_category_delete")
     * @Template()
     */
    public function eventCategoryDeleteAction($id)
    {
        $this->checkPermission();
        $evenTag = $this->getDoctrine()->getRepository('IngeEventBundle:EventTag')->findOneById($id);
        if ($evenTag) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($evenTag);
            $em->flush();
        }
        
        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    // COMPONENTS //////////////////////////////////////////
    ////////////////////////////////////////////////////////
    /**
     * @Template("IngeEventBundle:components:tag_name.html.twig")
     */
    public function fetchEventTagNameAction($tagIds) {
        $tagIds = json_decode($tagIds, true);
        $eventTagRepository = $this->getDoctrine()->getRepository('IngeEventBundle:EventTag');
        $data = $eventTagRepository->findById($tagIds);
        return array('data' => $data);
    }

    /**
     * @Template("IngeEventBundle:components:event_of_tag.html.twig")
     */
    public function eventByTagAction($tagId) {
        $events = $this->getDoctrine()->getRepository('IngeEventBundle:Event')->findEventByTagId($tagId);

        print_r($events);
        return array(
            'events' => $events
        );
    }
}

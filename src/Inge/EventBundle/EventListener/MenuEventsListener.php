<?php

namespace Inge\EventBundle\EventListener;

use Inge\AdminBundle\Event\ConfigureMenuEvent;

class MenuEventsListener
{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Inge\AdminBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureRoot($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $menu = $factory->createItem('event', array('route' => 'admin_event'));
        $menu->setAttribute('icon', 'fa fa-ticket');
        $menu->setExtra('orderNumber', 3);
        $parentMenu->addChild($menu);
    }
}
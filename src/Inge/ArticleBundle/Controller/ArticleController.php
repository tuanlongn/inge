<?php

namespace Inge\ArticleBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\ArticleBundle\Entity\Article;
use Inge\ArticleBundle\Entity\ArticleCategory;

class ArticleController extends AdminController
{

    // ARTICLE /////////////////////////////////////////////
    ////////////////////////////////////////////////////////

    /**
     * @Route("/admin/article", name="admin_article_list")
     * @Template("IngeArticleBundle::article_list.html.twig")
     */
    public function articleListAction()
    {
        $articles = $this->getDoctrine()->getRepository('IngeArticleBundle:Article')->findAll();

        return array(
            'articles' => $articles
        );
    }

    /**
     * @Route("/admin/article/add", name="admin_article_add")
     * @Template("IngeArticleBundle::article_form.html.twig")
     */
    public function articleAddAction(Request $request)
    {
        $article = new Article();

        if ($request->getMethod() === 'POST') {
            $article->setTitle($request->request->get('title'));
            $article->setContent($request->request->get('content'));

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $article->setStatus($status);

            $categoryId = $request->request->get('category');
            $category = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->findOneById($categoryId);
            $article->setCategory($category);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $this->flash('success', 'create successful.');

            return $this->redirect($this->generateUrl('admin_article_list'));
        }

        $categories = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->findAll();

        return array(
            'action' => 'add',
            'article' => $article,
            'categories' => $categories
        );
    }

    /**
     * @Route("/admin/article/{id}/edit", name="admin_article_edit")
     * @Template("IngeArticleBundle::article_form.html.twig")
     */
    public function articleEditAction(Request $request, $id)
    {
        $article = $this->getDoctrine()
                    ->getRepository('IngeArticleBundle:Article')
                    ->findOneById($id);
        $categories = $this->getDoctrine()
                    ->getRepository('IngeArticleBundle:ArticleCategory')
                    ->findAll();

        if ($request->getMethod() === 'POST') {
            $article->setTitle($request->request->get('title'));
            $article->setContent($request->request->get('content'));

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $article->setStatus($status);

            $categoryId = $request->request->get('category');
            $category = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->findOneById($categoryId);
            $article->setCategory($category);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $this->flash('success', 'update successful.');
        }
        
        return array(
            'action' => 'edit',
            'article' => $article,
            'categories' => $categories
        );
    }

    /**
     * @Route("/admin/article/{id}/delete", name="admin_article_delete")
     * @Template()
     */
    public function articleDeleteAction($id)
    {
        $article = $this->getDoctrine()
                    ->getRepository('IngeArticleBundle:Article')
                    ->findOneById($id);

        if ($article) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    // CATEGORY ////////////////////////////////////////////
    ////////////////////////////////////////////////////////

    /**
     * @Route("/admin/article/category/list", name="admin_article_category_list")
     * @Template("IngeArticleBundle::category_list.html.twig")
     */
    public function categoryListAction()
    {
        $categories = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->getSortedCategories();

        return array(
            'categories' => $categories
        );
    }

    /**
     * @Route("/admin/article/category/add", name="admin_article_category_add")
     * @Template("IngeArticleBundle::category_form.html.twig")
     */
    public function categoryAddAction(Request $request)
    {
        $this->checkPermission();

        $category = new ArticleCategory();

        if ($request->getMethod() === 'POST') {
            $category->setName($request->request->get('name'));
            $category->setParentId($request->request->get('parent_id'));

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $category->setStatus($status);

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->flash('success', 'create successful.');

            return $this->redirect($this->generateUrl('admin_article_category_list'));
        }

        $parentCates = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->findBy(array('parent_id' => 0));

        return array(
            'action' => 'add',
            'category' => $category,
            'parentCates' => $parentCates
        );
    }

    /**
     * @Route("/admin/article/category/{id}/edit", name="admin_article_category_edit")
     * @Template("IngeArticleBundle::category_form.html.twig")
     */
    public function categoryEditAction(Request $request, $id)
    {
        $this->checkPermission();

        $category = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->findOneById($id);

        if ($request->getMethod() === 'POST') {
            $category->setName($request->request->get('name'));
            $category->setParentId($request->request->get('parent_id'));

            $status = $request->request->get('status');
            if ($status) {
                $status = true;
            } else {
                $status = false;
            }
            $category->setStatus($status);

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->flash('success', 'update successful.');
        }

        $parentCates = $this->getDoctrine()->getRepository('IngeArticleBundle:ArticleCategory')->findBy(array('parent_id' => 0));
        if ($parentCates) {
            foreach ($parentCates as $k => $cate) {
                if ($category->getId() === $cate->getId()) {
                    unset($parentCates[$k]);
                }
            }
        }

        return array(
            'action' => 'edit',
            'category' => $category,
            'parentCates' => $parentCates
        );
    }

    /**
     * @Route("/admin/article/category/{id}/delete", name="admin_article_category_delete")
     * @Template()
     */
    public function categoryDeleteAction($id)
    {
        $category = $this->getDoctrine()
                    ->getRepository('IngeArticleBundle:ArticleCategory')
                    ->findOneById($id);

        if ($category) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }
        
        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }
}

<?php

namespace Inge\ArticleBundle\EventListener;

use Inge\AdminBundle\Event\ConfigureMenuEvent;

class MenuEventsListener
{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Inge\AdminBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureRoot($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $menu = $factory->createItem('article', array('route' => 'admin_article_list'));
        $menu->setAttribute('icon', 'fa fa-newspaper-o');
        $menu->setExtra('orderNumber', 6);
        $parentMenu->addChild($menu);
    }
}
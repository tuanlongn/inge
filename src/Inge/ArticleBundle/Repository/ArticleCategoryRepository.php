<?php

namespace Inge\ArticleBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ArticleCategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ArticleCategoryRepository extends EntityRepository
{
    public function getSortedCategories()
    {
        $query = $this->getEntityManager()->createQuery("
            SELECT c FROM IngeArticleBundle:ArticleCategory c
        ");
        $result = $query->getArrayResult();

        // sort
        $data = array();
        foreach ($result as $k => $v) {
            if ($v['parent_id'] === 0) {
                foreach ($v as $k1 => $v1) {
                    $data[$v['id']][$k1] = $v1;
                }
            } else {
                if (!array_key_exists($v['parent_id'], $data)) {
                    $data[$v['parent_id']] = array();
                }
                if (!array_key_exists('children', $data[$v['parent_id']])) {
                    $data[$v['parent_id']]['children'] = array();
                }
                $data[$v['parent_id']]['children'][$v['id']] = $v;
                
            }
        }
        return $data;
    }
}

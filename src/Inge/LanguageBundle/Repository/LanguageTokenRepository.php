<?php

namespace Inge\LanguageBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * LanguageTokenRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LanguageTokenRepository extends EntityRepository
{
    /**
     * Check token exists.
     */
    public function exists($token)
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT l
            FROM IngeLanguageBundle:LanguageToken l
            WHERE l.token = :token
        ');
        $query->setParameter('token', $token);
        $result = $query->getResult();
        return ($result) ? true : false;
    }

    public function findTranslationByToken($language) 
    {
        $query = $this->getEntityManager()->createQuery('
            SELECT ltoken, ltran.id, ltran.translation
            FROM IngeLanguageBundle:LanguageToken ltoken 
            LEFT JOIN IngeLanguageBundle:LanguageTranslation ltran
                WITH ltran.languageToken = ltoken.id
                AND ltran.language = :language
        ');
        $query->setParameter('language', $language);

        return $query->getResult();
    }
}

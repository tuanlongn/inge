<?php

namespace Inge\LanguageBundle\Helper;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class LanguageHelper
{
    /**
     * Create assemble translate file on ::app/Resources/translations/messages.[$locale].db.
     */
    public static function initTranslationAssembleFile($locale)
    {
        $langDir = __DIR__ . "/../../../../app/Resources/translations";

        $fs = new Filesystem();

        $fs->touch($langDir.'/messages.'.$locale.'.db');

        self::clearLanguageCache();
    }

    /**
     * Remove assemble tranlate file on ::app/Resources/translations/message.[$locale].db.
     */
    public static function removeTranslationAssembleFile($locale)
    {
        $langDir = __DIR__ . "/../../../../app/Resources/translations";

        $fs = new Filesystem();

        $fs->remove($langDir.'/messages.'.$locale.'.db');
        
        self::clearLanguageCache();
    }

    /**
     * Remove language in cache folder.
     */
    public static function clearLanguageCache()
    {
        $cacheDir = __DIR__ . "/../../../../app/cache";
        $cacheDevDir = $cacheDir . "/dev/translations";
        $cacheProdDir = $cacheDir . "/prod/translations";

        $fs = new Filesystem();

        $folders = array();
        if ($fs->exists($cacheProdDir)) {
            $folders[] = $cacheProdDir;
        }
        if ($fs->exists($cacheDevDir)) {
            $folders[] = $cacheDevDir;
        }
 
        $finder = new Finder();

        $finder->in($folders)->files();
 
        foreach($finder as $file) {
            unlink($file->getRealpath());
        }
    }
}
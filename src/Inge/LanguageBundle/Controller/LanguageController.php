<?php

namespace Inge\LanguageBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\LanguageBundle\Entity\Language;
use Inge\LanguageBundle\Entity\LanguageTranslation;

use Inge\LanguageBundle\Helper\LanguageHelper;

class LanguageController extends AdminController
{
    private $allLocales = array(
        'fr' => 'France',
        'it' => 'Italy',
        'es' => 'Spain',
        'de' => 'Germany',
        'en-GB' => 'United Kingdom',
        'en-US' => 'United States',
        'vi' => 'Vietnam'
    );

    /**
     * @Route("/admin/language", name="admin_language")
     * @Template("IngeLanguageBundle::language_list.html.twig")
     */
    public function languageListAction()
    {
        $this->checkPermission();

        $repository = $this->getDoctrine()
                           ->getRepository('IngeLanguageBundle:Language');
        $languageLists = $repository->findAll();
        $allLocales = $this->allLocales;
        foreach ($languageLists as $language) {
            if (array_key_exists($language->getLocale(), $allLocales)) {
                unset($allLocales[$language->getLocale()]);
            }
        }
        return array(
            'languageLists' => $languageLists,
            'allLocales' => $allLocales,
        );
    }

    /**
     * @Route("/admin/language/add", name="admin_language_add")
     * @Template()
     */
    public function languageAddAction(Request $request)
    {
        $this->checkPermission();

        if ($request->getMethod() === 'POST') {
            $locale = $request->request->get('locale');
            if ($locale) {
                $em = $this->getDoctrine()->getManager();

                $name = $this->allLocales[$locale];

                $language = new Language();
                $language->setLocale($locale);
                $language->setName($name);

                $em->persist($language);
                $em->flush();

                // clear cache
                LanguageHelper::initTranslationAssembleFile($locale);
            }
        }

        return $this->redirect($this->generateUrl('admin_language'));
    }

    /**
     * @Route("/admin/language/{id}/edit", name="admin_language_edit")
     * @Template("IngeLanguageBundle::language_edit.html.twig")
     */
    public function languageEditAction($id)
    {
        $this->checkPermission();

        $languageRepository = $this->getDoctrine()
                                   ->getRepository('IngeLanguageBundle:Language');
        $language = $languageRepository->findOneById($id);

        $tokens = $this->getDoctrine()
                       ->getRepository('IngeLanguageBundle:LanguageToken')
                       ->findTranslationByToken($language);

        return array(
            'language' => $language,
            'tokens' => $tokens,
        );
    }

    /**
     * @Route("/admin/language/update/{language_id}", name="admin_language_translation")
     * @Template()
     */
    public function LanguageTranslationAction($language_id, Request $request)
    {
        if ($request->get('translation_id') !== '') {
            $translation = $this->getDoctrine()
                            ->getRepository('IngeLanguageBundle:LanguageTranslation')
                            ->findOneById($request->get('translation_id'));
        } else {
            $translation = new LanguageTranslation();
        } 
        
        $language = $this->getDoctrine()
                    ->getRepository('IngeLanguageBundle:Language')
                    ->findOneById($language_id);
        $translation->setLanguage($language);

        $token = $this
                    ->getDoctrine()
                    ->getRepository('IngeLanguageBundle:LanguageToken')
                    ->findOneById($request->get('token_id'));
        $translation->setLanguageToken($token);

        $translation->setCatalogue('messages');
        $translation->setTranslation($request->get('translation'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($translation);
        $em->flush();

        // clear cache
        LanguageHelper::clearLanguageCache();

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    /**
     * @Route("/admin/language/{id}/delete", name="admin_language_delete")
     * @Template()
     */
    public function languageDeleteAction($id)
    {
        $this->checkPermission();

        $em = $this->getDoctrine()->getManager();

        $em->createQuery('
            DELETE IngeLanguageBundle:LanguageTranslation t WHERE t.language = :language'
        )->setParameter('language', $id)->execute();

        $languageRepository = $this->getDoctrine()
                                   ->getRepository('IngeLanguageBundle:Language');
        $language = $languageRepository->findOneById($id);

        $em->remove($language);
        $em->flush();

        // clear cache
        LanguageHelper::removeTranslationAssembleFile($language->getLocale());

        $data = array(
            'locale' => $language->getLocale(),
            'name' => $language->getName()
        );

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true,
            'data' => $data
        ));
        return $response;
    }
}

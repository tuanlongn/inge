<?php

namespace Inge\LanguageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\AdminBundle\Entity\Setting;
use Inge\LanguageBundle\Helper\LanguageHelper;

class CommonController extends Controller
{
    /**
     * @Route("/util/get_languages", name="get_languages")
     * @Template()
     */
    public function getLanguagesAction(Request $request)
    {
        $languages = $this->getDoctrine()
                    ->getRepository('IngeLanguageBundle:Language')
                    ->findAll();
        
        $data = array();
        foreach ($languages as $language) {
            $data[] = array(
                'locale' => $language->getLocale(),
                'name' => $language->getName()
            );
        }

        $response = new JsonResponse();
        $response->setData(array(
            'data' => $data
        ));
        return $response;
    }

    /**
     * @Route("/setting/get_language", name="get_setting_language")
     * @Template()
     */
    public function getSettingLanguageAction(Request $request)
    {
        $area = $request->get('area');
        $setting = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord($area, 'language');
        $data = array();
        if ($setting) {
            $language = $this->getDoctrine()->getRepository('IngeLanguageBundle:Language')->findByLocale($setting->getValue());

            $data['locale'] = $language->getLocale();
            $data['name'] = $language->getName();
        } else {
            $data['locale'] = $data['name'] = '';
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true,
            'data' => $data
        ));
        return $response;
    }

    /**
     * @Route("/admin/setting/language", name="admin_setting_language")
     * @Template()
     */
    public function settingLanguageAction(Request $request)
    {
        $area = $request->request->get('area');
        $setting = $this->getDoctrine()
                            ->getRepository('IngeAdminBundle:Setting')
                            ->findSettingRecord($area, 'language');

        if ($setting === false) {
            $setting = new Setting();
            $setting->setArea($area);
            $setting->setType('language');
        }

        $setting->setValue($request->request->get('locale'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($setting);
        $em->flush();

        $this->get('session')->set('_locale', $setting->getValue());

        // clear cache
        LanguageHelper::clearLanguageCache();

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }
}

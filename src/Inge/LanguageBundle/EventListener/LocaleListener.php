<?php

namespace Inge\LanguageBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Doctrine\ORM\EntityManager;

class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;
    private $repository;

    /**
     * @param %kernel.default_locale%
     * @param EntityManager $entityManager
     */
    public function __construct($defaultLocale = 'en', EntityManager $entityManager)
    {
        $this->defaultLocale = $defaultLocale;
        $this->repository = $entityManager->getRepository("IngeAdminBundle:Setting");
    }
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $currentRoute = $request->get('_route');

        if (strpos($currentRoute, 'admin') !== false) {
            $languageAdminSetting = $this->repository->findSettingRecord('backend', 'language');
            if ($languageAdminSetting) {
                $this->defaultLocale = $languageAdminSetting->getValue();
            }
        } else {
            $languageFrontendSetting = $this->repository->findSettingRecord('frontend', 'language');
            if ($languageFrontendSetting) {
                $this->defaultLocale = $languageFrontendSetting->getValue();
            }
        }

        if (!$request->hasPreviousSession()) {
            return;
        }

        if ($locale = $request->attributes->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {
            $request->getSession()->set('_locale', $this->defaultLocale);
            $request->setLocale($request->getSession()->get('_locale'));
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
        );
    }
}
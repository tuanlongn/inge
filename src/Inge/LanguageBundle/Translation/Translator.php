<?php

namespace Inge\LanguageBundle\Translation;

use Symfony\Bundle\FrameworkBundle\Translation\Translator as BaseTranslator;
use Inge\LanguageBundle\Entity\LanguageToken;

use Doctrine\ORM\EntityManager;

class Translator extends BaseTranslator
{
    /**
     * Overwrite trans function which is provider by translator component.
     */
    public function trans($token, array $parameters = array(), $domain = null, $locale = null)
    {
        $token = trim($token);
        //$this->checkTranslationExists($token, $locale);

        if (null === $locale) {
            $locale = $this->getLocale();
        }

        if (null === $domain) {
            $domain = 'messages';
        }

        if (!isset($this->catalogues[$locale])) {
            $this->loadCatalogue($locale);
        }

        return strtr($this->catalogues[$locale]->get((string) $token, $domain), $parameters);
    }

    private function checkTranslationExists($token)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $check = $em->getRepository('IngeLanguageBundle:LanguageToken')
                    ->exists($token);
                    
        if ($check === false) {
            $languageToken = new LanguageToken();
            $languageToken->setToken($token);
            $em->persist($languageToken);
            $em->flush();
        }
    }
}
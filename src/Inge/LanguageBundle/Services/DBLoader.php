<?php

namespace Inge\LanguageBundle\Services;

use Symfony\Component\Translation\Loader\LoaderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\MessageCatalogue;

class DBLoader implements LoaderInterface
{
    private $transaltionRepository;
    private $languageRepository;
 
    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->translationRepository = $entityManager->getRepository("IngeLanguageBundle:LanguageTranslation");
        $this->languageRepository = $entityManager->getRepository("IngeLanguageBundle:Language");
    }
 
    function load($resource, $locale, $domain = 'messages')
    {
        $catalogue = new MessageCatalogue($locale);
        //Load on the db for the specified local
        $language = $this->languageRepository->getLanguage($locale);
        if ($language !== 0) {
            $translations = $this->translationRepository->getTranslations($language, $domain);

            /**
             * @var $translation Inge\LanguageBundle\Entity\LanguageTranslation
             */
            foreach($translations as $translation) {
                $catalogue->set($translation->getLanguageToken()->getToken(), $translation->getTranslation(), $domain);
            }
        }

        return $catalogue;
    }
}
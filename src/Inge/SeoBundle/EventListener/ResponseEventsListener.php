<?php

namespace Inge\SeoBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Doctrine\ORM\EntityManager;
use \Wa72\HtmlPageDom\HtmlPage;

class ResponseEventsListener
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        /**
         * @var \Symfony\Component\HttpFoundation\Request $request
         */
        $request =  $event->getRequest();

        $route = $request->getUri();
        $response = $event->getResponse();
        
        // get seo definition
        $seo = $this->entityManager->getRepository('IngeSeoBundle:SeoRoute')->findOneByRoute($route);
        if ($seo) {
            $content = $response->getContent();

            $dom = new HtmlPage($content);
            $dom->setTitle($seo->getMetaTitle());
            $dom->setMeta('description', $seo->getMetaDescription());
            $dom->setMeta('keywords', $seo->getMetaKeywords());
            $dom->setMeta('language', $seo->getMetaLanguage());
            $dom->save();

            $response->setContent($dom);
        }
        
        // return response
        $event->setResponse($response);
    }
}
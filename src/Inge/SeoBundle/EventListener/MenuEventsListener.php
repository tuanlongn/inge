<?php

namespace Inge\SeoBundle\EventListener;

use Inge\AdminBundle\Event\ConfigureMenuEvent;

class MenuEventsListener
{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Inge\AdminBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureRoot($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $menu = $factory->createItem('seo', array('route' => 'admin_seo'));
        $menu->setAttribute('icon', 'fa fa-search-plus');
        $menu->setExtra('orderNumber', 8);
        $parentMenu->addChild($menu);
    }
}
<?php

namespace Inge\SeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="seo_routes")
 * @ORM\Entity(repositoryClass="Inge\SeoBundle\Repository\SeoRouteRepository")
 */
class SeoRoute
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $route;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $metaTitle;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $metaDescription;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $metaKeywords;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $metaLanguage;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return SeoRoute
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return SeoRoute
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return SeoRoute
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaLanguage
     *
     * @param string $metaLanguage
     * @return SeoRoute
     */
    public function setMetaLanguage($metaLanguage)
    {
        $this->metaLanguage = $metaLanguage;

        return $this;
    }

    /**
     * Get metaLanguage
     *
     * @return string 
     */
    public function getMetaLanguage()
    {
        return $this->metaLanguage;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return SeoRoute
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }
}

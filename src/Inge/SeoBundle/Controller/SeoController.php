<?php

namespace Inge\SeoBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\SeoBundle\Entity\SeoRoute;

class SeoController extends AdminController
{
    /**
     * @Route("/admin/seo", name="admin_seo")
     * @Template("IngeSeoBundle::seo_list.html.twig")
     */
    public function indexAction()
    {
        $this->checkPermission();
        
        $seos = $this->getDoctrine()->getRepository('IngeSeoBundle:SeoRoute')->findAll();

        return array(
            'seos' => $seos
        );
    }

    /**
     * @Route("/admin/seo/add", name="admin_seo_add")
     * @Template("IngeSeoBundle::seo_form.html.twig")
     */
    public function seoAddAction(Request $request)
    {
        $this->checkPermission();

        $seo = new SeoRoute();

        if ($request->getMethod() === 'POST') {
            $seo->setRoute($request->request->get('route'));
            $seo->setMetaTitle($request->request->get('meta_title'));
            $seo->setMetaDescription($request->request->get('meta_description'));
            $seo->setMetaKeywords($request->request->get('meta_keywords'));
            $seo->setMetaLanguage($request->request->get('meta_language'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($seo);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_seo'));
        }

        return array(
            'action' => 'add',
            'seo' => $seo
        );
    }

    /**
     * @Route("/admin/seo/{id}/edit", name="admin_seo_edit")
     * @Template("IngeSeoBundle::seo_form.html.twig")
     */
    public function seoEditAction(Request $request, $id)
    {
        $this->checkPermission();

        $seo = $this->getDoctrine()->getRepository('IngeSeoBundle:SeoRoute')->findOneById($id);

        if ($request->getMethod() === 'POST') {
            $seo->setRoute($request->request->get('route'));
            $seo->setMetaTitle($request->request->get('meta_title'));
            $seo->setMetaDescription($request->request->get('meta_description'));
            $seo->setMetaKeywords($request->request->get('meta_keywords'));
            $seo->setMetaLanguage($request->request->get('meta_language'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($seo);
            $em->flush();
        }

        return array(
            'action' => 'edit',
            'seo' => $seo
        );
    }

    /**
     * @Route("/admin/seo/{id}/delete", name="admin_seo_delete")
     * @Template()
     */
    public function seoDeleteAction($id)
    {
        $this->checkPermission();

        $seo = $this->getDoctrine()->getRepository('IngeSeoBundle:SeoRoute')->findOneById($id);
        if ($seo) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($seo);
            $em->flush();
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }
}

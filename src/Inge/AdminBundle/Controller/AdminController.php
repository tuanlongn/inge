<?php

namespace Inge\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AdminController extends Controller
{
    /**
     * Check permission
     * 
     * @throws AccessDeniedException
     */
    protected function checkPermission($role = 'ROLE_SUPER_ADMIN')
    {
        $roles = array();
        $roles[] = $role;
        $hasPermission = false;

        $request = $this->container->get('request');
        $route = $request->get('_route');
        $customRole = $this->getDoctrine()->getRepository('IngeAdminBundle:RoleRoute')->findOneBy(array('route' => $route));
        if ($customRole) {
            $tmp = $customRole->getRoles();
            foreach (json_decode($tmp, true) as $role) {
                $roles[] = $role;
            }
        }

        foreach ($roles as $role) {
            if (true === $this->get('security.context')->isGranted($role)) {
                $hasPermission = true;
                break;
            }
        }

        if ($hasPermission === false) {
            throw new AccessDeniedHttpException();
        }     
    }

    /**
     * Set flash message
     */
    public function flash($type, $message)
    {
        $msg = $this->get('translator')->trans($message);
        $this->get('session')->getFlashBag()->add($type, $msg);
    }
}

<?php

namespace Inge\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ErrorController extends Controller
{
    /**
     * @Route("/not_found", name="not_found")
     * @Template("IngeAdminBundle::not_found.html.twig")
     */
    public function notFoundAction()
    {
        return array();
    }
}

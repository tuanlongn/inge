<?php

namespace Inge\AdminBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\AdminBundle\Entity\MimeType;

class MimeTypeController extends AdminController
{
    /**
     * @Route("/admin/mime_type/list", name="admin_mime_type_list")
     * @Template("IngeAdminBundle::mime_type.html.twig")
     */
    public function mimeTypeListAction()
    {
        $mimeTypes = $this->getDoctrine()->getRepository('IngeAdminBundle:MimeType')->findAll();

        return array(
            'mimeTypes' => $mimeTypes
        );
    }

    /**
     * @Route("/admin/mime_type/add", name="admin_mime_type_add")
     * @Template()
     */
    public function mimeTypeAddAction(Request $request)
    {
        $extension = $request->request->get('extension');
        $mimeType = $request->request->get('mime_type');

        $mt = $this->getDoctrine()->getRepository('IngeAdminBundle:MimeType')->findOneBy(array('extension' => $extension));
        if (!$mt) {
            $mt = new mimeType();
            $mt->setExtension($extension);
            $mt->setMimeType($mimeType);

            $em = $this->getDoctrine()->getManager();
            $em->persist($mt);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_mime_type_list'));
    }

    /**
     * @Route("/admin/mime_type/{id}/edit", name="admin_mime_type_edit")
     * @Template()
     */
    public function mimeTypeEditAction(Request $request, $id)
    {
        $extension = $request->request->get('extension');
        $mimeType = $request->request->get('mime_type');

        $mt = $this->getDoctrine()->getRepository('IngeAdminBundle:MimeType')->findOneById($id);
        if ($mt) {
            $mt->setExtension($extension);
            $mt->setMimeType($mimeType);

            $em = $this->getDoctrine()->getManager();
            $em->persist($mt);
            $em->flush();
        }
        
        return $this->redirect($this->generateUrl('admin_mime_type_list'));
    }

    /**
     * @Route("/admin/mime_type/{id}/delete", name="admin_mime_type_delete")
     * @Template()
     */
    public function mimeTypeDeleteAction(Request $request, $id)
    {
        $mt = $this->getDoctrine()->getRepository('IngeAdminBundle:MimeType')->findOneById($id);
        if ($mt) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($mt);
            $em->flush();
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }
}
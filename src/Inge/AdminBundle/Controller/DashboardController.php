<?php

namespace Inge\AdminBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends AdminController
{
    /**
     * @Route("/admin/", name="admin_dashboard")
     * @Template("IngeAdminBundle::dashboard.html.twig")
     */
    public function dashboardAction()
    {
        $this->checkPermission();

        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('frontend_home'));
        }

        return array();
    }
}

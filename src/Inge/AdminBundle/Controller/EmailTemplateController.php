<?php

namespace Inge\AdminBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;

class EmailTemplateController extends AdminController
{
    /**
     * @Route("/admin/email/template/list", name="admin_email_template_list")
     * @Template("IngeAdminBundle::email_template_list.html.twig")
     */
    public function emailTemplateAction()
    {
        $this->checkPermission();

        $emailDir = __DIR__ . "/../../../../src/Inge/*/Resources/views/emails";

        $finder = new Finder();

        $finder->in(array($emailDir))->files();

        $data = array();
        foreach ($finder as $file) {
            $data[] = array(
                'name' => $file->getFilename(),
                'path' => $file->getRealpath()
            );
        }

        return array(
            'files' => $data
        );
    }

    /**
     * @Route("/admin/email/template/edit/{path}", name="admin_email_template_edit")
     * @Template("IngeAdminBundle::email_template_edit.html.twig")
     */
    public function emailTemplateEditAction(Request $request, $path)
    {
        $this->checkPermission();

        $filePath = urldecode($path);
        $fileName = basename($filePath);

        if ($request->getMethod() === 'POST') {
            $content = $request->request->get('content');
            file_put_contents($filePath, $content);

            $response = new JsonResponse();
            $response->setData(array(
                'status' => true
            ));
            return $response;
        }

        $content = file_get_contents($filePath, FILE_USE_INCLUDE_PATH);

        return array(
            'name' => $fileName,
            'path' => $path,
            'content' => $content
        );
    }

    /**
     * @Route("/admin/email/template/content", name="admin_email_template_content")
     * @Template()
     */
    public function emailTemplateContentAction(Request $request)
    {
        $this->checkPermission();
        
        $filePath = urldecode($request->get('path'));
        $content = file_get_contents($filePath, FILE_USE_INCLUDE_PATH);

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true,
            'data' => $content
        ));
        return $response;
    }
}
<?php

namespace Inge\AdminBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\AdminBundle\Entity\RoleRoute;

use Inge\AdminBundle\Entity\Setting;

class SettingController extends AdminController
{
    /**
     * @Route("/admin/setting", name="admin_setting")
     * @Template("IngeAdminBundle::setting.html.twig")
     */
    public function settingAction()
    {
        $this->checkPermission();

        $bundles = $this->getBundles();
        foreach ($bundles as $i => $bundle) {
            if ($this->container->hasParameter(strtolower($bundle).'.setting') === false) {
                unset($bundles[$i]);
            } else {
                $parameter = $this->container->getParameter(strtolower($bundle).'.setting');
                if ($parameter === false) {
                    unset($bundles[$i]);
                }
            }
        }
        
        return array(
            'bundles' => $bundles,
        );
    }

    /**
     * @Route("/admin/setting/get_value", name="admin_setting_get_value")
     * @Template()
     */
    public function getSettingValueAction(Request $request)
    {
        $area = $request->get('area');
        $type = $request->get('type');
        $setting = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord($area, $type);

        if ($setting) {
            $data = $setting->getValue();
        } else {
            $data = '';
        }
        
        $response = new JsonResponse();
        $response->setData(array(
            'status' => true,
            'data' => json_decode($data, true)
        ));
        return $response;
    }

    /**
     * @Route("/admin/setting/set_value", name="admin_setting_set_value")
     * @Template()
     */
    public function setSettingValueAction(Request $request)
    {
        $area = $request->request->get('area');
        $type = $request->request->get('type');
        $value = $request->request->get('value');

        $setting = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord($area, $type);
        if ($setting === false) {
            $setting = new Setting();
            $setting->setArea($area);
            $setting->setType($type);
        }
        if ($type === 'mime_type_upload') {
            $setting->setValue(json_encode($value, true));
        } else {
            $setting->setValue($value);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($setting);
        $em->flush();

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    /**
     * @Route("/admin/setting/role", name="admin_setting_role")
     * @Template("IngeAdminBundle:components:setting_role.html.twig")
     */
    public function settingRoleAction()
    {
        $this->checkPermission();

        $resources = $this->getPrivateResources();
        $roles = $this->get('inge_admin_helper.roles')->getRoles();
        $customRoles = $this->getDoctrine()
                        ->getRepository('IngeAdminBundle:Role')
                        ->findAll();
        foreach ($customRoles as $role) {
            $roles[] = $role->getName();
        }

        $roleRoutes = $this->getDoctrine()
                        ->getRepository('IngeAdminBundle:RoleRoute')
                        ->findAll();

        foreach ($roleRoutes as $roleRoute) {
            foreach ($resources as $i => $resource) {
                foreach ($resource as $route => $routeData) {
                    if ($roleRoute->getRoute() === $route) {
                        $resources[$i][$route]['roles'] = json_decode($roleRoute->getRoles(), true);
                    }
                }
            }
        }

        return array(
            'roles' => $roles,
            'resources' => $resources
        );
    }

    /**
     * @Route("/admin/setting/role/update", name="admin_setting_role_update")
     * @Template()
     */
    public function settingRoleUpdateAction(Request $request)
    {
        $route = $request->request->get('route');
        $role = $request->request->get('role');
        $checked = $request->request->get('checked');

        $roleRoute = $this->getDoctrine()->getRepository('IngeAdminBundle:RoleRoute')->findOneBy(array('route' => $route));
        if ($roleRoute) {
            $roles = json_decode($roleRoute->getRoles(), true);
            if ($checked) {
                $roles[] = $role;
            } else {
                foreach ($roles as $i => $roleName) {
                    if ($role === $roleName) {
                        unset($roles[$i]);
                    }
                }
            }
        } else {
            $roleRoute = new RoleRoute();
            $roleRoute->setRoute($route);
            $roles = array($role);
        }

        $roleRoute->setRoles(json_encode($roles));

        $em = $this->getDoctrine()->getManager();
        $em->persist($roleRoute);
        $em->flush();

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }

    /**
     * @Route("/admin/setting/test-send-email", name="admin_setting_test_send_email")
     * @Template()
     */
    public function testSendMailAction(Request $request)
    {
        $host = $request->request->get('host');
        $port = $request->request->get('port');
        $encryption = $request->request->get('encryption');
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $email_test = $request->request->get('email_test');

        if(!\Swift_Validate::email($email_test)) {
            $result = false;
            $msg = $this->get('translator')->trans('email test invalid.');
        } else {
            try {
                // create transport
                $transport = \Swift_SmtpTransport::newInstance($host, $port, $encryption)
                    ->setUsername($username)
                    ->setPassword($password);

                // create mailer using transport
                $mailer = \Swift_Mailer::newInstance($transport);

                // create a message
                $message = \Swift_Message::newInstance('Test send mail from INGE system')
                    ->setFrom(array('inge_project@test' => 'INGE PROJECT'))
                    ->setTo(array($email_test))
                    ->setBody('Here is an email testing');

                // send the message
                $result = $mailer->send($message);

                if ($result) {
                    $msg = $this->get('translator')->trans('send email successful.');

                    // save setting
                    $params = $request->request->all();
                    unset($params['email_test']);
                    $value = json_encode($params);
                    $area = 'all';
                    $type = 'email';

                    $setting = $this->getDoctrine()->getRepository('IngeAdminBundle:Setting')->findSettingRecord($area, $type);
                    if ($setting === false) {
                        $setting = new Setting();
                        $setting->setArea($area);
                        $setting->setType($type);
                    }

                    $setting->setValue($value);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($setting);
                    $em->flush();

                } else {
                    $msg = $this->get('translator')->trans('send email error.');
                }
            } catch (\Swift_TransportException $e) {
                $result = false;
                $msg = $this->get('translator')->trans('setting invalid, please try fix correct setting value.');
            }
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => $result,
            'msg' => $msg
        ));
        return $response;
    }

    ////////////////////////////////////////////////////////
    private function getBundles()
    {
        $bundles = array();
        $allBundles = $this->container->getParameter('kernel.bundles');
        foreach ($allBundles as $key => $val) {
            if (strpos($val, 'Inge') !== false) {
                $tmp = explode('\\', $val);
                $bundles[] = str_replace('Bundle', '', $tmp[1]);
            }
        }
        return $bundles;
    }

    private function getPrivateResources()
    {
        $privateResources = array();
        $allRoutes = $this->container->get('router')->getRouteCollection()->all();
        foreach ($allRoutes as $route => $data) {
            if (strpos($route, 'login') !== false || strpos($route, 'logout') !== false) {
                continue;
            } elseif (strpos($route, 'admin') !== false) {
                $controller = $data->getDefaults();
                $arr = explode('\\', $controller['_controller']);

                $bundle = str_replace('Bundle', '', $arr[1]);
                if (array_key_exists($bundle, $privateResources) === false) {
                    $privateResources[$bundle] = array();
                }
                
                $token = str_replace('admin', '', $route);
                $token = str_replace('_', ' ', $token);
                $privateResources[$bundle][$route] = array(
                    'name' => $this->get('translator')->trans($token)
                );
            }
        }
        return $privateResources;
    }
}

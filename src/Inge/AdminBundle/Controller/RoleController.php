<?php

namespace Inge\AdminBundle\Controller;

use Inge\AdminBundle\Controller\AdminController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Inge\AdminBundle\Entity\Role;

class RoleController extends AdminController
{
    /**
     * @Route("/admin/role/list", name="admin_role_list")
     * @Template("IngeAdminBundle::role_list.html.twig")
     */
    public function roleListAction()
    {
        $roles = $this->getDoctrine()->getRepository('IngeAdminBundle:Role')->findAll();

        return array(
            'roles' => $roles
        );
    }

    /**
     * @Route("/admin/role/add", name="admin_role_add")
     * @Template()
     */
    public function roleAddAction(Request $request)
    {
        $roleName = $request->request->get('role_name');
        $role = $this->getDoctrine()->getRepository('IngeAdminBundle:Role')->findOneBy(array('name' => $roleName));
        if ($role == false) {
            $role = new Role();
            $role->setName($roleName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_setting_role'));
    }

    /**
     * @Route("/admin/role/{id}/edit", name="admin_role_edit")
     * @Template()
     */
    public function roleEditAction(Request $request, $id)
    {
        $roleName = $request->request->get('role_name');
        $checkRoleExist = $this->getDoctrine()->getRepository('IngeAdminBundle:Role')->findOneBy(array('name' => $roleName));
        if ($checkRoleExist) {
            $msg = $this->get('translator')->trans('role name is existed. please use other name.');
            $this->get('session')->getFlashBag()->add('waining', $msg);
        } else {
            $role = $this->getDoctrine()->getRepository('IngeAdminBundle:Role')->findOneById($id);
            $role->setName($roleName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_role_read'));
    }

    /**
     * @Route("/admin/role/{id}/delete", name="admin_role_delete")
     * @Template()
     */
    public function roleDeleteAction(Request $request, $id)
    {
        $role = $this->getDoctrine()->getRepository('IngeAdminBundle:Role')->findOneById($id);
        if ($role) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($role);
            $em->flush();
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status' => true
        ));
        return $response;
    }
}
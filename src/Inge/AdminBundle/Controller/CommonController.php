<?php

namespace Inge\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommonController extends Controller
{
    /**
     * @Route("/common/get_roles", name="get_roles")
     * @Template()
     */
    public function getRolesAction(Request $request)
    {
        $roles = $this->get('inge_admin_helper.roles')->getRoles();
        $customRoles = $this->getDoctrine()->getRepository('IngeAdminBundle:Role')->findAll();
        foreach ($customRoles as $role) {
            $roles[] = $role->getName();
        }

        $q = $request->get('q');
        if ($q !== '') {
            foreach ($roles as $i => $role) {
                if (strpos(strtolower($role), strtolower($q)) === false) {
                    unset($roles[$i]);
                }
            }
        }
        
        $response = new JsonResponse();
        $response->setData(array(
            'data' => $roles
        ));
        return $response;
    }

    /**
     * @Route("/common/get_mime_types", name="get_mime_types")
     * @Template()
     */
    public function getMimeTypesAction(Request $request)
    {
        $mimeTypes = array();
        $mts = $this->getDoctrine()->getRepository('IngeAdminBundle:MimeType')->findAll();
        foreach ($mts as $mt) {
            $mimeTypes[] = array(
                'extension' => $mt->getExtension(),
                'mime_type' => $mt->getMimeType()
            );
        }

        $q = $request->get('q');
        if ($q !== '') {
            foreach ($mimeTypes as $i => $mt) {
                if (strpos(strtolower($mt['extension']), strtolower($q)) === false) {
                    unset($roles[$i]);
                }
            }
        }

        $response = new JsonResponse();
        $response->setData(array(
            'data' => $mimeTypes
        ));
        return $response;
    }
}

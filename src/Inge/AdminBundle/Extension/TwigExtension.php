<?php

namespace Inge\AdminBundle\Extension;

use \Twig_Extension;

class TwigExtension extends Twig_Extension
{
    public function getName() 
    {
        return 'twig.extension';
    }

    public function getFilters() {
        return array(
            'json_decode'   => new \Twig_Filter_Method($this, 'jsonDecode'),
        );
    }

    public function jsonDecode($str) {
        return json_decode($str);
    }
}
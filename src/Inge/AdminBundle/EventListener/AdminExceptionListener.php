<?php
 
namespace Inge\AdminBundle\EventListener;
 
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Router;
 
class AdminExceptionListener
{
    protected $_session;
    protected $_router;
    protected $_request;
 
    public function __construct(Session $session, Router $router, Request $request)
    {
        $this->_session = $session;
        $this->_router = $router;
        $this->_request = $request;
    }

    /**
     * Listen all exception throw from access admin.
     */
    public function onListenException(GetResponseForExceptionEvent $event) {
        $exception = $event->getException();
        if ($exception instanceof NotFoundHttpException) {
            $this->onNotFoundException($event);
        } else if ($exception instanceof AccessDeniedHttpException) {
            $this->onAccessDeniedException($event);
        }
    }

    /**
     * Not found exception handle.
     *
     * @param NotFoundHttpException
     * @return RedirectResponse
     */
    public function onNotFoundException($event)
    {
        $route = $this->_router->generate('not_found');
        $event->setResponse(new RedirectResponse($route));
    }
 
    /**
     * Access denied exception handle.
     *
     * @param AccessDeniedException
     * @return RedirectResponse
     */
    public function onAccessDeniedException($event)
    {
        $this->_session->getFlashBag()->add('error', 'Access Denied');

        if ($this->_request->headers->get('referer')) {
            $route = $this->_request->headers->get('referer');
        } else {
            $route = $this->_router->generate('admin_login');
        }

        $event->setResponse(new RedirectResponse($route));
    }
}
<?php

namespace Inge\AdminBundle\EventListener;

use Inge\AdminBundle\Event\MenuEvents;
use Inge\AdminBundle\Event\ConfigureMenuEvent;

class MenuEventListener
{
    protected $eventDispatcher;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param IngeAdminBundle\Events\ConfigureMenuEvent $event
     */
    public function onMenuConfigureRoot($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();
        
        $menu = $factory->createItem('dashboard', array('route' => 'admin_dashboard'));
        $menu->setAttribute('icon', 'fa fa-area-chart');
        $menu->setExtra('orderNumber', 1);
        $parentMenu->addChild($menu);

        $menu = $factory->createItem('settings', array('route' => 'admin_setting'));
        $this->eventDispatcher->dispatch(MenuEvents::CONFIGURE_SETTING_NODE, new ConfigureMenuEvent($factory, $menu));
        $menu->setAttribute('icon', 'fa fa-cog');
        $menu->setExtra('orderNumber', 9);
        $parentMenu->addChild($menu);
    }

    /**
     * @param Inge\AdminBundle\Event\ConfigureMenuEvent $event
     */
    public function menuConfigureSettingNode($event)
    {
        $parentMenu = $event->getMenu();
        $factory = $event->getFactory();

        $menu = $factory->createItem('roles', array('route' => 'admin_setting_role'));
        $menu->setAttribute('icon', 'fa fa-check-circle-o');
        $menu->setExtra('orderNumber', 2);
        $parentMenu->addChild($menu);

        $menu = $factory->createItem('mime-type', array('route' => 'admin_mime_type_list'));
        $menu->setAttribute('icon', 'fa fa-file');
        $menu->setExtra('orderNumber', 3);
        $parentMenu->addChild($menu);

        $menu = $factory->createItem('email template', array('route' => 'admin_email_template_list'));
        $menu->setAttribute('icon', 'fa fa-envelope');
        $menu->setExtra('orderNumber', 4);
        $parentMenu->addChild($menu);
    }
}
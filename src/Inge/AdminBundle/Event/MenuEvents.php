<?php

namespace Inge\AdminBundle\Event;

final class MenuEvents
{
    /**
     * @var string
     */
    const CONFIGURE_ROOT = 'admin.menuConfigureRoot';

    /**
     * @var string
     */
    const CONFIGURE_SETTING_NODE = 'admin.menuConfigureSettingNode';
}
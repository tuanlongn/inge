### INSTALL MANUAL ###

# Composer:
- $ SYMFONY_ENV=prod php composer.phar install --no-dev --optimize-autoloader

# Create/Update database:
- $ php app/console doctrine:database:create
- $ php app/console doctrine:database:drop
- $ php app/console doctrine:schema:create
- $ php app/console doctrine:schema:drop
- $ php app/console doctrine:schema:update --force

# Dump asset:
- $ php app/console assetic:dump